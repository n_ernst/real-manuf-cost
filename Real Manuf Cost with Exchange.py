import os
import sys
import pyodbc
import pandas as pd
import logging
from datetime import datetime


def make_conn():
    '''
    creates connection to database
    Returns:
        - conn - connection
    '''
    conn = pyodbc.connect(
        'Driver={SQL Server};'
        'Server=WATSQLDEV16;'
        'Database=PartCost;'
        'user=dmtest;'
        'password=dmtest;'
                        )

    return conn


def get_data(IZPN, PMPLNT, real_cost):
    '''
    Retrieves the child parts from DE100M along with
    PMPLNT, PZQTY(DE120M), PSDCC(DE120M).
    Nomenclatures:
        - IZPN - Part number from DE100M
        - PMPLNT - Primary plant from DE100M
        - PZQTY - Part quantity from DE120M
        - PSDCC - Decimal place from DE120M
        - MUCFM - Manufacturing cost from DE110M
    Arguments:
        - IZPN - Part number
        - PMPLNT - Primary plant code
        - real_cost - Calculated manufacturing cost of product
    Returns:
        - real_cost - the real cost to manufacture parts
    '''
    conn = make_conn()

    # pulls all necessary variables from DE100m, joining APMPASTP,
    # XREFVendorNamePrimaryPlant, and DE120M
    sql_query = "SELECT [IZPN] \
        ,[PMBTQT] \
        ,D.[PMPLNT] \
        ,[IZQTY] \
        ,[CMDESC] \
        ,[CMDES2] \
        ,[MUTMS] \
        ,[AMVENN] \
        ,CASE \
                WHEN APMASTP.VENNAM IS NULL THEN '' \
                ELSE APMASTP.VENNAM \
        END AS VENNAM \
            ,MUCFM \
            ,PMSUOM \
            ,PMFCCD \
            ,X.PMPLNT as PSPLNT \
            ,APMASTP.VENNO \
            ,M.PZQTY \
        ,M.PSDCC \
    FROM [PartCost].[dbo].[DE100M] D \
    LEFT JOIN APMASTP ON amvenn=venno \
    LEFT JOIN XREFVendorNamePrimaryPlant X ON APMASTP.VENNO = X.VENNO \
    LEFT JOIN DE120M m ON m.PSPLNT = d.PMPLNT AND izpn = PZCPN\
    WHERE PZPPN = '" + IZPN + "' AND PSPLNT = '" + PMPLNT +"'\
    AND D.PMPLNT = '" + PMPLNT +"'\
    AND D.IZACD = '1'\
    ORDER BY 1"


    df = pd.read_sql_query(sql_query, conn)
    conn.close()

    real_cost = get_cost(df)

    return real_cost


def look_ahead (IZPN, PMPLNT):
    '''
    Runs the query from get_data for the level ahead of the current part to
    see if recursion needs to continue or if we have reached the bottom leaf.
    Nomenclatures:
        - IZPN - Part number from DE100M
        - PMPLNT - Primary plant from DE100M
        - PZQTY - Part quantity from DE120M
        - PSDCC - Decimal place from DE120M
        - MUCFM - Manufacturing cost from DE110M
    Arguments:
        - IZPN - Part number
        - PMPLNT - Primary plant code
    Returns:
        - 1 if a child part exists
        - 0 if it's a leaf
    '''
    conn = make_conn()
    # queries one level ahead of the current part to see if recursion needs to
    # continue or if we have reached the bottom leaf.
    sql_query = "SELECT [IZPN] \
        ,[PMBTQT] \
        ,D.[PMPLNT] \
        ,[IZQTY] \
        ,[CMDESC] \
        ,[CMDES2] \
        ,[MUTMS] \
        ,[AMVENN] \
        ,CASE \
                WHEN APMASTP.VENNAM IS NULL THEN '' \
                ELSE APMASTP.VENNAM \
        END AS VENNAM \
            ,MUCFM \
            ,PMSUOM \
            ,PMFCCD \
            ,X.PMPLNT as PSPLNT \
            ,APMASTP.VENNO \
            ,M.PZQTY\
        ,M.PSDCC\
    FROM [PartCost].[dbo].[DE100M] D \
    LEFT JOIN APMASTP ON amvenn=venno \
    LEFT JOIN XREFVendorNamePrimaryPlant X ON APMASTP.VENNO = X.VENNO \
    LEFT JOIN DE120M m ON m.PSPLNT = d.PMPLNT AND izpn = PZCPN\
    WHERE PZPPN = '" + IZPN + "' AND PSPLNT = '" + PMPLNT +"'\
    AND D.PMPLNT = '" + PMPLNT +"'\
    AND D.IZACD = '1'\
    ORDER BY 1"

    df = pd.read_sql_query(sql_query, conn)

    if df.empty:
        return 0

    return 1


def insert_new_records(conn, IZPN, cost):
    '''
    Inserts records into SQL database table that are not already in the table.
    Nomenclatures:
        - IZPN - Part number from DE100M
        - Real_Cost - real manufacturing cost of the product
        - DateTime - timestamp of insert
    Arguments:
        - conn - connect to database
        - IZPN - Part number
        - cost - calculated real cost
    Returns:
        - Row insert into SQL database
    '''
    cursor = conn.cursor()
    # inserts into table
    cursor.execute('INSERT INTO dbo.TestTable('
                                        '[IZPN],'
                                        '[Real_Cost],'
                                        '[DateTime])'
                                        'values ('
                                        '?, ?, ?)'
                                        '', IZPN,
                                        cost,
                                        datetime.now(),
                                    )
    conn.commit()
    cursor.close()
    conn.close()


def config_lookup(IZPN, real_cost):
    '''
    Checks DE100M for parts in BM_LEVEL0 that are not the top level.
    Nomenclatures:
        - IZPN - Part number from DE100M
        - PMPLNT - Primary plant from DE100M
        - BMLVL - Config level
    Arguments:
        - IZPN - Part number
        - Real_Cost - real manufacturing cost of the product
    Returns:
        - real_cost - the real cost to manufacture parts
    '''
    cost = 0

    conn = make_conn()
    # checks DE100M for parts in BM_LEVEL0 that are not the top level
    sql_query = "SELECT IZPN \
        ,PMPLNT \
        ,MUCFM \
        FROM DE100M \
        WHERE IZPN IN (SELECT BMCPN \
            FROM [dbo].[BM_LEVEL0] \
            WHERE BMPPN ='" + IZPN + "' \
                AND BMLVL != '0') \
        AND PMPLNT IN (SELECT BMPLNT \
            FROM [dbo].[BM_LEVEL0] \
            WHERE BMPPN ='" + IZPN + "' \
                AND BMLVL != '0')"    
    df = pd.read_sql_query(sql_query, conn)
    conn.close()

    for index, row in df.iterrows():

        if look_ahead(row['IZPN'], row['PMPLNT']):
            cost = get_data(row['IZPN'], row['PMPLNT'], cost)
            real_cost = cost + real_cost
        else:
            cost = row['MUCFM']
            conn = make_conn()

            sql_query = "SELECT IZPN \
                    FROM testtable \
                    WHERE IZPN = '{}'".format(row['IZPN'])
            testdata = pd.read_sql_query(sql_query, conn)

            if testdata.empty:
                insert_new_records(conn, row['IZPN'], cost)
            real_cost = cost + real_cost

    return real_cost


def get_exchange_rate(VENNO):
    '''
    If the VENNO exchange rate is not 1.00, the true exchange rate is returned
    Nomenclatures:
        - VENNO - vendor sellinig part
        - ExchangeRate - currency exchange rate against USD
    Arguments:
        - VENNO - vendor selling part
    Returns:
        - exchange_rate against USD

    '''
    exchange_rate = 1.00

    conn = make_conn()

    sql_query = "SELECT [ExchangeRate] \
                From XREFVendorNamePrimaryPlant \
                Where VENNO = '" + VENNO + "'";

    df = pd.read_sql_query(sql_query, conn)

    if not df.empty:
        exchange_rate = df["ExchangeRate"].iloc[0]
    return exchange_rate


def get_real_cost(PZQTY, PSDCC, MUCFM, AMVENN, PMPLNT, PSPLNT):
    '''
    Real manufacturing cost of product is determined.
    Arguments:
        - PZQTY - Quantity of child parts in parent part
        - PSDCC - Location of decimal in PZQTY
        - MUCFM - Reported part cost
        - AMVENN - Originating vendor number
        - PMPLNT - Primary plant code
        - PSPLNT - Child part's plant
    Returns:
        - real_cost - the real cost to manufacture parts
    '''
    conn = make_conn()

    sql_query = "SELECT [Currency] \
                From XREFVendorNamePrimaryPlant \
                Where VENNO = '" + AMVENN + "'";

    df = pd.read_sql_query(sql_query, conn)

    sql_query = "SELECT [ExchangeRate] \
                From XREFVendorNamePrimaryPlant \
                Where PMPLNT = '" + PMPLNT + "'";

    df1 = pd.read_sql_query(sql_query, conn)

    # real cost is determined against quantity, decimal spacing,
    # and exchange rate (if applicable)
    if PSPLNT is not None:
        if PSPLNT != PMPLNT:

            # USD to USD
            if df["Currency"].iloc[0] != 'USD':

                real_cost = PZQTY / (10**PSDCC) * MUCFM * 1

            # USD to foreign currency
            else:
                real_cost = PZQTY / (10**PSDCC) * MUCFM / get_exchange_rate (AMVENN)

        # Foreign currency to USD
        else:
            real_cost = PZQTY / (10**PSDCC) * MUCFM * get_exchange_rate (AMVENN)

    # Exchange rate for if PSPLNT is none
    else:
        real_cost = PZQTY / (10**PSDCC) * MUCFM / df1['ExchangeRate'].iloc[0]

    return real_cost


def get_cost(df):
    '''
    Recursivly call the get_data function to execute level by level to
    calculate the sum at every leaf of the tree. Once the bottom level of
    leaves is summed call back up the tree to get the sum of each WATLOW
    manufactured part along the tree.
    Nomenclatures:
        - IZPN - Part number from DE100M
        - PMPLNT - Primary plant from DE100M
        - BMLVL - Config level
        - PZQTY - Quantity of child parts in parent part
        - PSDCC - Location of decimal in PZQTY
        - MUCFM - Reported part cost
        - AMVENN - Originating vendor number
        - PMPLNT - Parent part's plant
        - PSPLNT - Child part's plant
    Arguments:
        - df - dataframe containing all children of the parent part
    Returns:
        - real_cost - real cost of the manufactured components
    '''
    cost = 0
    real_cost = 0

    for index, row in df.iterrows():

        if pd.isnull(row['PSPLNT']):
            psplnt = (row['PMPLNT'])
        else:
            psplnt = (row['PSPLNT'])

        conn = make_conn()
        sql_query = "SELECT IZPN \
                    FROM testtable \
                    WHERE IZPN = '{}'".format(row['IZPN'])

        testdata = pd.read_sql_query(sql_query, conn)

        cost = get_real_cost(row['PZQTY'], row['PSDCC'], row['MUCFM'], row['AMVENN'], row['PMPLNT'], row['PSPLNT'])

        if look_ahead (row['IZPN'], psplnt):
            cost = get_real_cost(row['PZQTY'], row['PSDCC'], get_data(row['IZPN'], psplnt, real_cost), row['AMVENN'], row['PMPLNT'], row['PSPLNT'])

            if testdata.empty:
                insert_new_records(conn, row['IZPN'], cost/row['PZQTY'])



        else:
            sql_query = "SELECT CIITEM \
                        ,CIPN \
                        FROM OP210M \
                        WHERE CIITEM = '{}' \
                        AND CICUST IN (SELECT * \
                        FROM WatlowCustomerID) \
                        AND CIPN != CIITEM".format(row['IZPN'])
            customer_item_df = pd.read_sql_query(sql_query, conn)
            if not customer_item_df.empty:
                sql_query = "SELECT VENNO \
                            FROM XREFVendorNamePrimaryPlant \
                            WHERE VENNO  = '" + row['AMVENN'] + "'"
                venno_check = pd.read_sql_query(sql_query, conn)

                if venno_check.empty:
                    cost = get_real_cost(row['PZQTY'], row['PSDCC'], row['MUCFM'], row['AMVENN'], row['PMPLNT'], row['PSPLNT'])

                else:
                    cost=get_real_cost(row['PZQTY'], row['PSDCC'], get_data(customer_item_df['CIPN'].iloc[0], psplnt, real_cost), row['AMVENN'], row['PMPLNT'], row['PSPLNT'])

                if testdata.empty:
                    insert_new_records(conn, row['IZPN'], cost/row['PZQTY'])

        real_cost = cost + real_cost
    return real_cost


def main(IZPN, PMPLNT, ConfigCode):
    '''
    Calls the correct starting locaion based on config code from BMLEVEL_0
    Arguments:
        - df - dataframe containing all children of the parent part
        - IZPN - Part number from DE100M
        - ConfigCode - Config code of part from BM_LEVEL0
    Returns:
        - real_cost - real cost of the manufactured components
    '''

    if not ConfigCode == "Non-Configured":
        real_cost = config_lookup(ConfigCode, 0)

    else:
        real_cost = get_data(IZPN, PMPLNT, 0)

    print(real_cost)
    return real_cost


if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2], sys.argv[3])
    #main('CMPSTK     ', 'COL', 'L120200C2-04W4W')
    #main('2128-8706', 'STL', 'Non-Configured' )